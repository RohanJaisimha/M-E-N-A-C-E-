reset:
	rm -rf Data.txt
	rm -rf Data.png
	rm -rf Combinations.json
	python3 makeCombinations.py

format:
	black *.py
