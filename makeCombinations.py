from constants import COMBINATIONS_JSON_FILENAME
from constants import NUMBER_OF_POSSIBLE_MOVES_TO_BEGIN_WITH
from constants import ALL_POSSIBLE_MOVES
from utilities import isSolved
from utilities import loadJson
from utilities import saveJson


# counts the number of times a character appears in a series of text
# Eg. count("Hello World!",'o')=2
def count(text, char):
    count = 0
    for i in text:
        if i == char:
            count += 1
    return count


# removes all combinations that are already solved
def removeAllSolvedOnes():
    combinations = loadJson(COMBINATIONS_JSON_FILENAME)

    solvedCombinations = set()

    for combination in combinations:
        if isSolved(combination):
            solvedCombinations.add(combination)

    for solvedCombination in solvedCombinations:
        del combinations[solvedCombination]

    saveJson(combinations, COMBINATIONS_JSON_FILENAME)


# removes all combinations where the number of 'x' is not equal to the number of 'o'
def removeBadOnes():
    combinations = loadJson(COMBINATIONS_JSON_FILENAME)

    badCombinations = set()

    for combination in combinations:
        count_1 = count(combination, "1")
        count_2 = count(combination, "2")
        if count_1 != count_2:
            badCombinations.add(combination)

    for badCombination in badCombinations:
        del combinations[badCombination]

    saveJson(combinations, COMBINATIONS_JSON_FILENAME)


def addPossibleMoves():
    combinations = loadJson(COMBINATIONS_JSON_FILENAME)

    for combination in combinations:
        combinations[combination] = {}
        for j in range(9):
            if combination[j] == "0":
                combinations[combination][
                    ALL_POSSIBLE_MOVES[j]
                ] = NUMBER_OF_POSSIBLE_MOVES_TO_BEGIN_WITH

    saveJson(combinations, COMBINATIONS_JSON_FILENAME)


def createDataFile():
    fout = open("Data.txt", "w")
    fout.write("Number of games played\tNumber of moves possible for first move\n")
    fout.write(f"{0}\t{9 * NUMBER_OF_POSSIBLE_MOVES_TO_BEGIN_WITH}\n")
    fout.close()


def makeCombinations():
    combinations = {}

    for i1 in range(3):
        for i2 in range(3):
            for i3 in range(3):
                for i4 in range(3):
                    for i5 in range(3):
                        for i6 in range(3):
                            for i7 in range(3):
                                for i8 in range(3):
                                    for i9 in range(3):
                                        combinations[
                                            f"{i1}{i2}{i3}{i4}{i5}{i6}{i7}{i8}{i9}"
                                        ] = None

    saveJson(combinations, COMBINATIONS_JSON_FILENAME)


def main():
    makeCombinations()
    removeBadOnes()
    removeAllSolvedOnes()
    addPossibleMoves()
    createDataFile()


main()
