# MENACE

* M.E.N.A.C.E. - Machine Educable Noughts and Crosses Engine
* Machine learns best strategy for Tic Tac Toe (Noughts and Crosses).
* Uses a "technology" similar to what Donald Michie did in 1960 (https://www.dropbox.com/s/ycsycu0l01g9643/DonaldMichie.pdf?dl=0). 
* For more information, see this video by Matt Parker (https://www.youtube.com/watch?v=R9c-_neaxeU).
* After playing against MENACE about 200 times, it will consistently draw against or beat the user.

# Files
* makeCombinations.py is a python program that generates all possible 2423 states of a tic tac toe board.
* Data.txt is a text file that contains data of MENACE's progress.
* Combinations.json is a json file that contains all of the aforementioned 2423 states of the tic tac toe board, along with the empty squares for each state.
* MENACE.py is a python program that allows the user to play against MENACE.
* Data.pdf contains the visualization of Data.txt
* MENACETrainer.py is a python program that plays games against MENACE to quickly train it
* utilities.py is a python program that houses all the functions shared between programs
* constants.py is a python program that houses all the constants shared between programs

# How to Run
* Clone/download the repo onto your device.
* Then, install the required modules for this program. You can do this by typing `pip install -r requirements.txt`
* Get started by running makeCombinations.py once. You can do this by typing `python3 makeCombinations.py`. Doing so will create Data.txt and Combinations.txt
* Then, run MENACE.py. You can do this by typing `python3 MENACE.py`. Follow the instructions on the console until you have completed a game. MENACE.py also creates Data.pdf. The graph is plotted using Matplotlib.
* If at any point you want to reset MENACE, simply type `make reset`